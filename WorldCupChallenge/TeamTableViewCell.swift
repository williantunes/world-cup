//
//  TeamTableViewCell.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/19/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {

    @IBOutlet weak var teamImage: UIImageView!
    
    @IBOutlet weak var teamLabel: UILabel!
    
    @IBOutlet weak var confederationLabel: UILabel!
    
    @IBOutlet weak var teamCellBackground: UIView!
}
