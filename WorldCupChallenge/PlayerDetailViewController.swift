//
//  PlayerDetailViewController.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/21/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class PlayerDetailViewController: UIViewController {
    
    var player: Player!
    
    @IBOutlet weak var playerPhoto: UIImageView!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var goalsLabel: UILabel!
    
    @IBOutlet weak var nameHeaderLabel: UILabel!
    
    @IBOutlet weak var nameDetailLabel: UILabel!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var birthView: UIView!
    
    @IBOutlet weak var heightView: UIView!
    
    @IBOutlet weak var positionLabel: UILabel!
    
    @IBOutlet weak var numberLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heightLabel.text = String(player.height) + "cm"
        goalsLabel.text = String(player.goals)
        nameHeaderLabel.text = player.name
        nameDetailLabel.text = player.name
        switch player.position {
        case .coach: positionLabel.text = "Coach"
        case .goalkeeper: positionLabel.text = "Goalkeeper"
        case .defender: positionLabel.text = "Defender"
        case .midfielder: positionLabel.text = "Midfielder"
        case .forward: positionLabel.text = "Forward"
        }
        
        backgroundView.layer.cornerRadius = 12
        
        playerPhoto.image = UIImage(named: "BR_\(player.number)")
        playerPhoto.layer.borderWidth = 5.0
        playerPhoto.layer.masksToBounds = false
        let color = UIColor(red: 0.2890625, green: 0.5625, blue: 0.8828125, alpha: 1.0)
        playerPhoto.layer.borderColor = color.cgColor
        playerPhoto.layer.cornerRadius = playerPhoto.frame.size.height/2
        playerPhoto.clipsToBounds = true
        playerPhoto.layer.cornerRadius = playerPhoto.frame.size.height/2
        
        numberLabel.text = String(player.number)
        
        birthView.layer.cornerRadius = 12
        birthView.layer.borderWidth = 2.0
        birthView.layer.borderColor = UIColor.white.cgColor
        birthView.backgroundColor = UIColor.clear
        
        heightView.layer.cornerRadius = 12
        heightView.layer.borderWidth = 2.0
        heightView.layer.borderColor = UIColor.white.cgColor
        heightView.backgroundColor = UIColor.clear
    }
    
}
