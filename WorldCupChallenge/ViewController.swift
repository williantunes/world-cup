//
//  ViewController.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/19/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var teamsTable: UITableView!
    
    @IBAction func backToTeams (_ segue: UIStoryboardSegue) {
        
    }
    
    let model = WorldCupModel.teams()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teamsTable.dataSource = self
        teamsTable.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.teamsTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.teams.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as! TeamTableViewCell
        
        let team = model.teams[indexPath.row]
        
        cell.teamLabel.text = team.name
        cell.teamImage.image = UIImage(named: team.name)
        cell.teamImage.layer.borderWidth = 1.0
        cell.teamImage.layer.masksToBounds = false
        cell.teamImage.layer.borderColor = UIColor.clear.cgColor
        cell.teamImage.layer.cornerRadius = cell.teamImage.frame.size.height/2
        cell.teamImage.clipsToBounds = true
        cell.teamImage.layer.cornerRadius = cell.teamImage.frame.size.height/2
        cell.confederationLabel.text = team.confederation
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        cell.teamCellBackground.layer.cornerRadius = 12
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("Will display", cell)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = "Teams"
        return title
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let team = model.teams[indexPath.row]
        performSegue(withIdentifier: "showPlayers", sender: team)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PlayersViewController, segue.identifier == "showPlayers" {
            guard let team = sender as? Team else { return }
            vc.team = team
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.white
        let header = view as! UITableViewHeaderFooterView
        let color = UIColor(red: 0.05859375, green: 0.26953125, blue: 0.5078125, alpha: 1.0)
        header.textLabel?.textColor = color
    }
    
}

