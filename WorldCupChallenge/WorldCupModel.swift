//
//  WorldCupModel.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/19/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import Foundation

struct WorldCupModel {
    
    var players = [Player]()
    var teams = [Team]()
    
    static public func teams() -> WorldCupModel {
        var model = WorldCupModel()
        
        model.players.append(Player(name: "Alisson", age: 25, height: 193, goals: 0, position: .goalkeeper, number: 1))
        model.players.append(Player(name: "Neymar Jr.", age: 26, height: 175, goals: 55, position: .forward, number: 10))
        model.players.append(Player(name: "Cassio", age: 31, height: 195, goals: 0, position: .goalkeeper, number: 16))
        model.players.append(Player(name: "Ederson", age: 24, height: 188, goals: 0, position: .goalkeeper, number: 23))
        model.players.append(Player(name: "Thiago Silva", age: 33, height: 183, goals: 0, position: .defender, number: 2))
        model.players.append(Player(name: "Miranda", age: 33, height: 186, goals: 0, position: .defender, number: 3))
        model.players.append(Player(name: "Pedro Geromel", age: 32, height: 190, goals: 0, position: .defender, number: 4))
        model.players.append(Player(name: "Filipe Luis", age: 32, height: 182, goals: 0, position: .defender, number: 6))
        model.players.append(Player(name: "Marcelo", age: 30, height: 174, goals: 0, position: .defender, number: 12))
        model.players.append(Player(name: "Marquinhos", age: 24, height: 183, goals: 0, position: .defender, number: 13))
        model.players.append(Player(name: "Danilo", age: 26, height: 184, goals: 0, position: .defender, number: 14))
        model.players.append(Player(name: "Fagner", age: 29, height: 168, goals: 0, position: .defender, number: 22))
        model.players.append(Player(name: "Casemiro", age: 26, height: 185, goals: 0, position: .midfielder, number: 5))
        model.players.append(Player(name: "Renato Augusto", age: 30, height: 186, goals: 0, position: .midfielder, number: 8))
        model.players.append(Player(name: "Phillipe Coutinho", age: 26, height: 172, goals: 1, position: .midfielder, number: 11))
        model.players.append(Player(name: "Paulinho", age: 29, height: 181, goals: 0, position: .midfielder, number: 15))
        model.players.append(Player(name: "Fernandinho", age: 33, height: 179, goals: 0, position: .midfielder, number: 17))
        model.players.append(Player(name: "Fred", age: 25, height: 169, goals: 0, position: .midfielder, number: 18))
        model.players.append(Player(name: "Willian", age: 29, height: 175, goals: 0, position: .midfielder, number: 19))
        model.players.append(Player(name: "Douglas Costa", age: 27, height: 182, goals: 0, position: .midfielder, number: 7))
        model.players.append(Player(name: "Gabriel Jesus", age: 21, height: 175, goals: 0, position: .forward, number: 9))
        model.players.append(Player(name: "Roberto Firmino", age: 26, height: 181, goals: 0, position: .forward, number: 20))
        model.players.append(Player(name: "Taison", age: 30, height: 172, goals: 0, position: .forward, number: 21))
        model.players.append(Player(name: "Tite", age: 57, height: 184, goals: 0, position: .coach, number: 0))
        
        model.teams.append(Team(players: model.players, name: "Argentina", confederation: "CONMEBOL"))
        model.teams.append(Team(players: model.players, name: "Australia", confederation: "AFC"))
        model.teams.append(Team(players: model.players, name: "Belgium", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Brazil", confederation: "CONMEBOL"))
        model.teams.append(Team(players: model.players, name: "Colombia", confederation: "CONMEBOL"))
        model.teams.append(Team(players: model.players, name: "Costa Rica", confederation: "CONCACAF"))
        model.teams.append(Team(players: model.players, name: "Croatia", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Denmark", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Egypt", confederation: "CAF"))
        model.teams.append(Team(players: model.players, name: "England", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "France", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Germany", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Iceland", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Iran", confederation: "AFC"))
        model.teams.append(Team(players: model.players, name: "Japan", confederation: "AFC"))
        model.teams.append(Team(players: model.players, name: "Mexico", confederation: "CONCACAF"))
        model.teams.append(Team(players: model.players, name: "Morocco", confederation: "CAF"))
        model.teams.append(Team(players: model.players, name: "Nigeria", confederation: "CAF"))
        model.teams.append(Team(players: model.players, name: "Panama", confederation: "CONCACAF"))
        model.teams.append(Team(players: model.players, name: "Peru", confederation: "CONMEBOL"))
        model.teams.append(Team(players: model.players, name: "Poland", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Portugal", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Russia", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Saudi Arabia", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Senegal", confederation: "CAF"))
        model.teams.append(Team(players: model.players, name: "Serbia", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "South Korea", confederation: "AFC"))
        model.teams.append(Team(players: model.players, name: "Spain", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Sweden", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Switzerland", confederation: "UEFA"))
        model.teams.append(Team(players: model.players, name: "Tunisia", confederation: "AFC"))
        model.teams.append(Team(players: model.players, name: "Uruguay", confederation: "CONMEBOL"))
        
        return model
    }

}
