//
//  PlayersViewController.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/20/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class PlayersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var playersTable: UITableView!
    
    @IBOutlet weak var teamLabel: UILabel!
    
    @IBAction func backToPlayers(_ segue: UIStoryboardSegue) {
        
    }

    var team: Team!
    
    var positions: [Player.Position] { return [.coach, .goalkeeper, .defender, .midfielder, .forward] }
    var coach: [Player] { return team.players.filter({ $0.position == .coach })}
    var goalkeepers: [Player] { return team.players.filter({ $0.position == .goalkeeper })}
    var defenders: [Player] { return team.players.filter({ $0.position == .defender })}
    var midfielders: [Player] { return team.players.filter({ $0.position == .midfielder })}
    var forwards: [Player] { return team.players.filter({ $0.position == .forward })}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playersTable.dataSource = self
        playersTable.delegate = self
        teamLabel.text = "\(team.name)'s National Team"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return positions.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return goalkeepers.count
        case 1: return defenders.count
        case 2: return midfielders.count
        case 3: return forwards.count
        case 4: return coach.count
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Goalkeeper"
        case 1: return "Defender"
        case 2: return "Midfielder"
        case 3: return "Forward"
        case 4: return "Coach"
        default: return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as! PlayerTableViewCell
        
        let player: Player!
        
        switch indexPath.section {
        case 0: player = goalkeepers[indexPath.row]
        case 1: player = defenders[indexPath.row]
        case 2: player = midfielders[indexPath.row]
        case 3: player = forwards[indexPath.row]
        case 4: player = coach[indexPath.row]
        default: player = nil
        }
        
        cell.nameLabel.text = player.name
        cell.ageLabel.text = String(player.age)
        cell.playerPhoto.image = UIImage(named: "BR_\(player.number)")
        cell.playerPhoto.layer.borderWidth = 1.0
        cell.playerPhoto.layer.masksToBounds = false
        cell.playerPhoto.layer.borderColor = UIColor.clear.cgColor
        cell.playerPhoto.layer.cornerRadius = cell.playerPhoto.frame.size.height/2
        cell.playerPhoto.clipsToBounds = true
        cell.playerPhoto.layer.cornerRadius = cell.playerPhoto.frame.size.height/2
        
        cell.numberLabel.text = String(player.number)
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        cell.playerCellBackground.layer.cornerRadius = 12
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 16,y: 16), radius: CGFloat(16), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2.0
        
        cell.numberBackground.layer.addSublayer(shapeLayer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.white
        let header = view as! UITableViewHeaderFooterView
        let color = UIColor(red: 0.2890625, green: 0.5625, blue: 0.8828125, alpha: 1.0)
        header.textLabel?.textColor = color
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let player: Player!
        
        switch indexPath.section {
        case 0: player = goalkeepers[indexPath.row]
        case 1: player = defenders[indexPath.row]
        case 2: player = midfielders[indexPath.row]
        case 3: player = forwards[indexPath.row]
        case 4: player = coach[indexPath.row]
        default: player = nil
        }
        performSegue(withIdentifier: "goToDetail", sender: player)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PlayerDetailViewController, segue.identifier == "goToDetail" {
            guard let player = sender as? Player else { return }
            vc.player = player
        }
    }

}
