//
//  PlayerTableViewCell.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/20/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var playerPhoto: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var playerCellBackground: UIView!
    
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var numberBackground: UIView!
    
}
