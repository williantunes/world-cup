//
//  Team.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/19/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import Foundation

struct Team {
    
    let players: [Player]
    let name: String
    let confederation: String
    
}
