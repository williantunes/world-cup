//
//  Player.swift
//  WorldCupChallenge
//
//  Created by Willian Antunes on 6/19/18.
//  Copyright © 2018 Willian Antunes. All rights reserved.
//

import Foundation

struct Player {
    
    enum Position {
        case coach
        case goalkeeper
        case defender
        case midfielder
        case forward
    }
    
    let name: String
    let age: Int
//    let birth = Date()
    let height: Int
    let goals: Int
    let position: Position
    let number: Int
    
}
